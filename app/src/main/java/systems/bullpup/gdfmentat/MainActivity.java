package systems.bullpup.gdfmentat;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private EditText attacks;
    private EditText quality;
    private EditText ap;
    private EditText blast;
    private EditText deadly;
    private CheckBox poison;
    private CheckBox rending;

    private EditText defence;
    private CheckBox cover;
    private CheckBox regen;
    private CheckBox stealth;

    private Button mentat;

    private TextView output;

    private Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        random = new Random();
        setContentView(R.layout.activity_main);

        attacks = findViewById(R.id.attacks);
        quality = findViewById(R.id.quality);
        ap = findViewById(R.id.ap);
        blast = findViewById(R.id.blast);
        deadly = findViewById(R.id.deadly);
        poison = findViewById(R.id.poison);
        rending = findViewById(R.id.rending);

        defence = findViewById(R.id.defence);
        cover = findViewById(R.id.cover);
        regen = findViewById(R.id.regen);
        stealth = findViewById(R.id.stealth);

        output = findViewById(R.id.output);

        mentat = findViewById(R.id.mentat);
        mentat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mentat();
            }
        });
    }

    private void mentat() {
        int attacks = Integer.parseInt(this.attacks.getText().toString().isEmpty() ? "1" : this.attacks.getText().toString());
        int quality = Integer.parseInt(this.quality.getText().toString().isEmpty() ? "4" : this.quality.getText().toString());
        int ap = Integer.parseInt(this.ap.getText().toString().isEmpty() ? "0" : this.ap.getText().toString());
        int blast = Integer.parseInt(this.blast.getText().toString().isEmpty() ? "1" : this.blast.getText().toString());
        int deadly = Integer.parseInt(this.deadly.getText().toString().isEmpty() ? "1" : this.deadly.getText().toString());
        boolean poison = this.poison.isChecked();
        boolean rending = this.rending.isChecked();

        int defence = Integer.parseInt(this.defence.getText().toString().isEmpty() ? "4" : this.defence.getText().toString());
        boolean cover = this.cover.isChecked();
        boolean regen = this.regen.isChecked();
        boolean stealth = this.stealth.isChecked();

        int[] results = new int[50];

        String resultText = "\n";
        int samples = 0;
        long startTime = System.currentTimeMillis();

        while(true) {
            int hitCount = 0;
            int rendingHitCount = 0;

            for(int a = 0; a < attacks; a++) {
                int attackRoll = rollD6();
                if (rending && attackRoll == 6) rendingHitCount++;
                else if ((attackRoll >= quality + (cover ? 1 : 0) + (stealth ? 1 : 0)) || attackRoll == 6)
                    hitCount += ((poison && attackRoll == 6) ? blast * 3 : blast);
            }

            int defenceCount = 0;
            for(int h = 0; h < hitCount; h++) {
                int defenceRoll = rollD6();
                if(defenceRoll == 6 || (defenceRoll >= defence + ap)) defenceCount++;
                else if(regen && rollD6() > 4) {
                    defenceCount++;
                }
            }
            for(int h = 0; h < rendingHitCount; h++) {
                int defenceRoll = rollD6();
                if(defenceRoll == 6 || defenceRoll >= defence + 4) defenceCount++;
            }

            int wounds = ((hitCount+rendingHitCount) - defenceCount) * deadly;

            if(wounds<50)results[wounds]++;
            samples++;
            if(System.currentTimeMillis()-startTime > 1000) break;
        }
        resultText += "Samples: "+samples+"\n";

        for(int i = 0; i < results.length; i++) {
            if(results[i] == 0) continue;
            long value = Math.round((double)results[i] / (double)samples * 35d);
            String row = " "+i;
            row = row.substring(row.length()-2);
            resultText += row+" [";
            for(long f = 0; f < value; f++) {
                resultText += "*";
            }
            for(long j = 0; j < 35l - value; j++) {
                resultText += "_";
            }
            String percentage = ((((double)results[i] / (double)samples)*100d)+"");
            if(percentage.length()>4) percentage = percentage.substring(0,4);
            resultText += "]"+percentage+"\n";
        }
        output.setText(resultText);
    }

    private int rollD6() {
        return random.nextInt(6)+1;
    }
}
